@extends('master')

@section('content')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="animated fadeIn">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Tìm kiếm</strong>
                            </div>
                            <div class="modal-body">
                                <form action="#" class="form-horizontal">
                                    <div class="row form-group">
                                        <div class="col-12 col-md-5">
                                            <select name="selectSm" id="selectSm" class="form-control-sm form-control">
                                            <option value="0">--- Tất cả trình ký ---</option>
                                            <option value="1">Bàn giao công việc</option>
                                            <option value="2">Đề xuất nhân sự</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-md-2"><span class="btn btn-primary btn-sm">Tìm kiếm</span></div>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>



                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Danh sách luồng duyệt</strong>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th class="serial">#</th>
                                            <th>Mã</th>
                                            <th>Trình ký</th>
                                            <th>Người xử lý</th>
                                            <th>Ngày xử lý</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="serial">1</td>
                                            <td>2/2020/CV-Icom</td>
                                            <td>Bàn giao công việc</td>
                                            <td>Quản trị viên</td>
                                            <td>08:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-info btn-sm">Tải xuống</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="serial">2</td>
                                            <td>2/2020/CV-Icom</td>
                                            <td>Bàn giao công việc</td>
                                            <td>Quản trị viên</td>
                                            <td>08:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-info btn-sm">Tải xuống</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="serial">3</td>
                                            <td>1/2020/CV-Icom</td>
                                            <td>Giới thiệu nhân sự</td>
                                            <td>Quản trị viên</td>
                                            <td>08:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-info btn-sm">Tải xuống</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="serial">4</td>
                                            <td>1/2020/CV-Icom</td>
                                            <td>Giới thiệu nhân sự</td>
                                            <td>Quản trị viên</td>
                                            <td>08:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-info btn-sm">Tải xuống</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="serial">5</td>
                                            <td>1/2019/CV-Icom</td>
                                            <td>Giới thiệu nhân sự</td>
                                            <td>Quản trị viên</td>
                                            <td>08:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-info btn-sm">Tải xuống</span>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div> <!-- /.table-stats -->

                            <div class="card-header">
                                <div class="row form-group">
                                    <div class="col-12 col-md-8"><strong class="card-title">Đang xem 6 trong tổng số 66</strong></div>
                                    <div class="col-12 col-md-4">
                                        <span class="btn btn-info btn-sm">1</span>
                                        <span class="btn btn-info btn-sm">4</span>
                                        <span class="btn btn-info btn-sm">5</span>
                                        <span class="btn btn-info btn-sm">6</span>
                                        <span class="btn btn-primary btn-sm">7</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    


                </div>
            </div>
	</div>
	
</div>
@stop