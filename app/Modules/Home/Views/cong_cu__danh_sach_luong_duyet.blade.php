
@extends('master')

@section('content')
@section('content')
<div class="row">
    <div class="col-lg-12">
    <div class="animated fadeIn">

                <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title" id="mediumModalLabel"><b>Phân quyền</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                    </div>
                    <div class="modal-body">
                        <form action="#" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="multiple-select" class=" form-control-label">Chọn phòng ban</label></div>
                                <div class="col col-md-9">
                                <select name="multiple-select" id="multiple-select" multiple="" class="form-control">
                                    <option value="0">--- Chọn phòng ban ---</option>
                                    <option value="1">Giám đốc</option>
                                    <option value="2">|----Phòng kinh doanh</option>
                                    <option value="3">|----|----Kinh doanh quốc tế</option>
                                    <option value="4">|----|----Chăm sóc khách hàng</option>
                                    <option value="5">|----Đào tạo</option>
                                    <option value="6">|----|----Đào tạo nội bộ</option>
                                    <option value="7">|----|----Hợp tác đào tạo</option>
                                </select>
                                <small class="form-text text-muted">Toàn bộ nhân sự trong phòng sẽ có quyền phê duyệt ngang nhau</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="multiple-select" class=" form-control-label">Chọn người dùng</label></div>
                                <div class="col col-md-9">
                                <select name="multiple-select" id="multiple-select" multiple="" class="form-control">
                                <option value="1">Giám đốc</option>
                                <option value="2">Thư ký</option>
                                <option value="3">Nhân sự 1</option>
                                <option value="4">Nhân sự 2</option>
                                </select>
                                <small class="form-text text-muted">Các nhân sự được chọn sẽ có quyền phê duyệt ngang nhau</small>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                    <button type="button" class="btn btn-primary">Thực hiện</button>
                    </div>
                    </div>
                    </div>
                </div>

                <div class="modal fade" id="mediumApprove" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title" id="mediumModalLabel"><b>Xóa luồng duyệt</b></h4>
                    </div>
                    <div class="modal-body">
                        <p>Xóa luồng duyệt này?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                        <button type="button" class="btn btn-primary">Thực hiện</button>
                    </div>
                    </div>
                    </div>
                </div>

                <div class="modal fade" id="upApproved" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title" id="mediumModalLabel"><b>Cập nhật thứ tự</b></h4>
                    </div>
                    <div class="modal-body">
                        <form action="#" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col-12 col-md-9">
                                    <select name="selectSm" id="selectSm" class="form-control-sm form-control">
                                    <option value="0">Chọn tứ tự</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                    <button type="button" class="btn btn-primary">Thực hiện</button>
                    </div>
                    </div>
                    </div>
                </div>

                <div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title" id="mediumModalLabel"><b>Thêm luồng duyệt</b></h4>
                    </div>
                    <div class="modal-body">
                        <form action="#" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="multiple-select" class=" form-control-label">Chọn trình ký</label></div>
                                <div class="col-12 col-md-9">
                                    <select name="selectSm" id="selectSm" class="form-control-sm form-control">
                                    <option value="0">Chọn trình ký</option>
                                    <option value="1">Bàn giao công việc</option>
                                    <option value="2">Đề xuất nhân sự</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="multiple-select" class=" form-control-label">Tên luồng duyệt</label></div>
                                <div class="col-12 col-md-9"><input type="tex" id="name-input" name="name-input" placeholder="Nhập tên luồng duyệt" class="form-control"></div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="multiple-select" class=" form-control-label">Chọn phòng ban</label></div>
                                <div class="col col-md-9">
                                <select name="multiple-select" id="multiple-select" multiple="" class="form-control">
                                    <option value="0">--- Chọn phòng ban ---</option>
                                    <option value="1">Giám đốc</option>
                                    <option value="2">|----Phòng kinh doanh</option>
                                    <option value="3">|----|----Kinh doanh quốc tế</option>
                                    <option value="4">|----|----Chăm sóc khách hàng</option>
                                    <option value="5">|----Đào tạo</option>
                                    <option value="6">|----|----Đào tạo nội bộ</option>
                                    <option value="7">|----|----Hợp tác đào tạo</option>
                                </select>
                                <small class="form-text text-muted">Toàn bộ nhân sự trong phòng sẽ có quyền phê duyệt ngang nhau</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="multiple-select" class=" form-control-label">Chọn người dùng</label></div>
                                <div class="col col-md-9">
                                <select name="multiple-select" id="multiple-select" multiple="" class="form-control">
                                <option value="1">Giám đốc</option>
                                <option value="2">Thư ký</option>
                                <option value="3">Nhân sự 1</option>
                                <option value="4">Nhân sự 2</option>
                                </select>
                                <small class="form-text text-muted">Các nhân sự được chọn sẽ có quyền phê duyệt ngang nhau</small>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                    <button type="button" class="btn btn-primary">Thực hiện</button>
                    </div>
                    </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#addUser">Thêm luồng duyệt</button>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Tìm kiếm</strong>
                            </div>
                            <div class="modal-body">
                                <form action="#" class="form-horizontal">
                                    <div class="row form-group">
                                        <div class="col-12 col-md-5">
                                            <select name="selectSm" id="selectSm" class="form-control-sm form-control">
                                            <option value="0">--- Tất cả trình ký ---</option>
                                            <option value="1">Bàn giao công việc</option>
                                            <option value="2">Đề xuất nhân sự</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-md-5"><input type="tex" id="name-input" name="name-input" placeholder="Nhập tên luồng duyệt" class="form-control"></div>
                                        <div class="col-12 col-md-2"><span class="btn btn-primary btn-sm">Tìm kiếm</span></div>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>



                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Danh sách luồng duyệt</strong>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th class="serial">#</th>
                                            <th>Trình ký</th>
                                            <th>Luồng duyệt</th>
                                            <th>Thứ tự</th>
                                            <th>Người tạo</th>
                                            <th>Ngày tạo</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="serial">1</td>
                                            <td>Bàn giao công việc</td>
                                            <td>Bàn giao dự án</td>
                                            <td>1</td>
                                            <td>Quản trị viên</td>
                                            <td>08:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-info btn-sm" data-toggle="modal" data-target="#upApproved">Đổi thứ tự</span>
                                                <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mediumModal">Phân quyền</span>
                                                <span class="btn btn-danger btn-sm" data-toggle="modal" data-target="#mediumApprove">Xóa</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="serial">2</td>
                                            <td>Bàn giao công việc</td>
                                            <td>Bàn giao tài sản</td>
                                            <td>2</td>
                                            <td>Quản trị viên</td>
                                            <td>08:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-info btn-sm" data-toggle="modal" data-target="#upApproved">Đổi thứ tự</span>
                                                <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mediumModal">Phân quyền</span>
                                                <span class="btn btn-danger btn-sm" data-toggle="modal" data-target="#mediumApprove">Xóa</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="serial">3</td>
                                            <td>Bàn giao công việc</td>
                                            <td>Chốt bảo hiểm</td>
                                            <td>3</td>
                                            <td>Quản trị viên</td>
                                            <td>08:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-info btn-sm" data-toggle="modal" data-target="#upApproved">Đổi thứ tự</span>
                                                <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mediumModal">Phân quyền</span>
                                                <span class="btn btn-danger btn-sm" data-toggle="modal" data-target="#mediumApprove">Xóa</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="serial">4</td>
                                            <td>Bàn giao công việc</td>
                                            <td>Thanh toán thôi việc</td>
                                            <td>4</td>
                                            <td>Quản trị viên</td>
                                            <td>08:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-info btn-sm" data-toggle="modal" data-target="#upApproved">Đổi thứ tự</span>
                                                <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mediumModal">Phân quyền</span>
                                                <span class="btn btn-danger btn-sm" data-toggle="modal" data-target="#mediumApprove">Xóa</span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="serial">5</td>
                                            <td>Giới thiệu nhân sự</td>
                                            <td>Giám định thông tin</td>
                                            <td>1</td>
                                            <td>Quản trị viên</td>
                                            <td>08:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-info btn-sm" data-toggle="modal" data-target="#upApproved">Đổi thứ tự</span>
                                                <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mediumModal">Phân quyền</span>
                                                <span class="btn btn-danger btn-sm" data-toggle="modal" data-target="#mediumApprove">Xóa</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="serial">6</td>
                                            <td>Giới thiệu nhân sự</td>
                                            <td>Phỏng vấn</td>
                                            <td>2</td>
                                            <td>Quản trị viên</td>
                                            <td>08:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-info btn-sm" data-toggle="modal" data-target="#upApproved">Đổi thứ tự</span>
                                                <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mediumModal">Phân quyền</span>
                                                <span class="btn btn-danger btn-sm" data-toggle="modal" data-target="#mediumApprove">Xóa</span>
                                            </td>
                                        </tr>

                                        
                                    </tbody>
                                </table>
                            </div> <!-- /.table-stats -->

                            <div class="card-header">
                                <div class="row form-group">
                                    <div class="col-12 col-md-8"><strong class="card-title">Đang xem 6 trong tổng số 66</strong></div>
                                    <div class="col-12 col-md-4">
                                        <span class="btn btn-info btn-sm">1</span>
                                        <span class="btn btn-info btn-sm">4</span>
                                        <span class="btn btn-info btn-sm">5</span>
                                        <span class="btn btn-info btn-sm">6</span>
                                        <span class="btn btn-primary btn-sm">7</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    


                </div>
            </div>

</div>
</div>
</div>
@stop



       