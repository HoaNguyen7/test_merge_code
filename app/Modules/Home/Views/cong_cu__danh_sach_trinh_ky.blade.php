@extends('master')

@section('content')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="animated fadeIn">

                <div class="modal fade" id="mediumApprove" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
                    style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mediumModalLabel">Xóa trình ký</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Bạn có muốn xóa trình ký này?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                <button type="button" class="btn btn-primary">Thực hiện</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
                    style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="mediumModalLabel"><b>Thêm trình ký</b></h4>
                            </div>
                            <div class="modal-body">
                                <form action="#" class="form-horizontal">
                                    <div class="row form-group">
                                        <div class="col-12 col-md-9"><input type="tex" id="name-input" name="name-input"
                                                placeholder="Nhập mã trình ký" class="form-control"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12 col-md-9"><input type="tex" id="name-input" name="name-input"
                                                placeholder="Nhập tên trình ký" class="form-control"></div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                <button type="button" class="btn btn-primary">Thực hiện</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUser">Thêm trình
                            ký</button>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Danh sách trình ký</strong>
                            </div>
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th class="serial">#</th>
                                            <th>Mã</th>
                                            <th>Tên</th>
                                            <th>Số bước duyệt</th>
                                            <th>Người tạo</th>
                                            <th>Ngày tạo</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="serial">1</td>
                                            <td>KEY1</td>
                                            <td>Đề xuất đăng tin tuyển dụng</td>
                                            <td>4</td>
                                            <td>admin</td>
                                            <td>06:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target="#mediumApprove">Xóa</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="serial">2</td>
                                            <td>KEY2</td>
                                            <td>Đề xuất nhân sự</td>
                                            <td>5</td>
                                            <td>admin</td>
                                            <td>06:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target="#mediumApprove">Xóa</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="serial">3</td>
                                            <td>KEY3</td>
                                            <td>Bàn giao thiết bị</td>
                                            <td>2</td>
                                            <td>admin</td>
                                            <td>06:00 01-04-2020</td>
                                            <td>
                                                <span class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target="#mediumApprove">Xóa</span>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div> <!-- /.table-stats -->
                        </div>
                    </div>



                </div>
            </div>

        </div>

    </div>
@stop
