<?php

namespace App\Modules\Home\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;//slug
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    public function index(){
    	 return view('Home::index');
    }
    public function level_action(){
        
            $data_level=DB::table('level')
            ->select('level.id','level.name_level','level.id_ref')
            ->orderByDesc('id_ref')
            ->paginate(5);
            //return response($data_level);
            return view('Home::level_action')->with('data_level',$data_level);
    }
    public function book_action(){
            $data_book=DB::table('book')
            ->select('book.id','book.type_book','book.name_book','book.type_name','book.id_level')
            ->paginate(5);
            //return response($data_level);
            return view('Home::book_action')->with('data_book',$data_book);
    }
    public function chapter_action(){
        $data_chapter=DB::table('chapter')
        ->select('chapter.id','chapter.id_book','chapter.name_chapter')
        ->paginate(10);
        //return response($data_chapter);
        return view('Home::chapter_action')->with('data_chapter',$data_chapter);
        
    }
    public function lesson_action(){
        $data_lesson=DB::table('lesson')
        ->select('lesson.id','lesson.name_lesson','lesson.id_chapter')
        ->paginate(10);
        return view('Home::lesson_action')->with('data_lesson',$data_lesson);

    	
    }
    public function content_action(){
        $data_content=DB::table('content')
        ->select('content.id','content.title','content.id_lesson','content.content')
        ->paginate(10);
        return view('Home::content_action')->with('data_content',$data_content);

    	
    }
    public function editLevel(Request $request){
        $id_level=$request->id;
        $data_level=DB::table('level')
        ->select('level.id','level.id_ref','level.name_level')
        ->where('level.id','=',$id_level)
        ->get();
        return response($data_level);
    }

    public function editBook(Request $request){
        $id_book=$request->id;
        $data_book=DB::table('book')
        ->select('book.id','book.id_level','book.name_book','book.type_book','book.type_name')
        ->where('book.id','=',$id_book)
        ->get();
        return response($data_book);
    }
    public function editChapter(Request $request){
        $id_chapter=$request->id;
        $data_chapter=DB::table('chapter')
        ->select('chapter.id','chapter.id_book','chapter.name_chapter')
        ->where('chapter.id','=',$id_chapter)
        ->get();
        return response($data_chapter);
        
    }
    public function editLesson(Request $request){
        $id_lesson=$request->id;
        $data_lesson=DB::table('lesson')
        ->select('lesson.id','lesson.id_chapter','lesson.name_lesson')
        ->where('lesson.id','=',$id_lesson)
        ->get();
        return response($data_lesson);
    }
    public function editContent(Request $request){
        $id_content=$request->id;
        $data_content=DB::table('content')
        ->select('content.id','content.title','content.content','content.id_lesson')
        ->where('content.id','=',$id_content)
        ->get();
        return response($data_content);
        
    }

    public function updateLevel(Request $request){
        $rules = array(
            'level_id'   =>'required',
            'level_name'    =>  'required',
            'level_id_ref'     =>  'required'
        );
        $error = Validator::make($request->all(), $rules);
        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }
       else{
            $name = $rules['level_name'];
            $icons = $rules['level_id_ref'];
           // echo $request->level_id;
            DB::table('level')  
            ->where('level.id', $request->level_id)
            ->update(['level.id_ref' => $request->level_id_ref,
                    'level.name_level'=>$request->level_name]);
            return response()->json([  $request->all() ]);
       }
    }
    public function updateBook(Request $request){
        $rules = array(
            'book_id'   =>'required',
            'book_type'    =>  'required',
            'book_name_type'     =>  'required',
            'book_name'     =>  'required',
            'book_id_level'     =>  'required',
        );
        $error = Validator::make($request->all(), $rules);
        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }
       else{
            DB::table('book')  
            ->where('book.id', $request->book_id)
            ->update(['book.id_level'=>$request->book_id_level,
                    'book.name_book'=>$request->book_name,
                    'book.type_book' => $request->book_type,
                    'book.type_name'=>$request->book_name_type]
                    );
            return response()->json([  $request->all() ]);
       }
    }

    
    
}























































// public function cong_cu__danh_sach_luong_duyet(){
    // 	return view('Home::cong_cu__danh_sach_luong_duyet');
    // }
    // public function cong_cu__danh_sach_trinh_ky(){
    // 	return view('Home::cong_cu__danh_sach_trinh_ky');
    // }
    // public function luu_tru__lich_su_duyet(){
    // 	return view('Home::luu_tru__lich_su_duyet');
    // }
    // public function luu_tru__trinh_ky_da_duyet(){
    //     return view('Home::luu_tru__trinh_ky_da_duyet');
    // }