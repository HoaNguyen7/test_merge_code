<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Modules\Home\Controllers\HomeController;


$namespace = 'App\Modules\Home\Controllers';

Route::group(array('middleware' => 'web', 'module' => 'Home', 'namespace' => $namespace), function () {
    Route::group(['prefix' => '/'], function () {
        // Route::get('cong_cu__danh_sach_luong_duyet.html' , 'HomeController@cong_cu__danh_sach_luong_duyet' );
        // Route::get('cong_cu__danh_sach_trinh_ky.html','HomeController@cong_cu__danh_sach_trinh_ky');
        // Route::get('index.html','HomeController@index');
        // Route::get('luu_tru__lich_su_duyet.html','HomeController@luu_tru__lich_su_duyet');
        // Route::get('luu_tru__trinh_ky_da_duyet.html','HomeController@luu_tru__trinh_ky_da_duyet');
        // Route::get('lesson_action.html','HomeController@lesson_action');
        // Route::get('content_action.html','HomeController@content_action');
        // Route::get('level_action.html','HomeController@level_action');
        // Route::get('tac_vu__tao_tac_vu.html','HomeController@tac_vu__tao_tac_vu');
        // Route::get('tac_vu__tu_choi.html','HomeController@tac_vu__tu_choi');        
    });
});


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Auth::routes();


Route::middleware(['auth'])->group(function () {
    Route::get('cong_cu__danh_sach_luong_duyet.html' , [HomeController::class,'cong_cu__danh_sach_luong_duyet'] );
    Route::get('cong_cu__danh_sach_trinh_ky.html',[HomeController::class,'cong_cu__danh_sach_trinh_ky']);
    //Route::get('/home',[HomeController::class,'index'])->name('home');
    Route::get('luu_tru__lich_su_duyet.html',[HomeController::class,'luu_tru__lich_su_duyet']);
    Route::get('luu_tru__trinh_ky_da_duyet.html',[HomeController::class,'luu_tru__trinh_ky_da_duyet']);
    Route::get('lesson_action.html',[HomeController::class,'lesson_action']);
    Route::get('content_action.html',[HomeController::class,'content_action']);
    Route::get('level_action.html',[HomeController::class,'level_action']);
    Route::get('book_action.html',[HomeController::class,'book_action']);
    Route::get('chapter_action.html',[HomeController::class,'chapter_action']);
    Route::get('editlevel/{id}',[HomeController::class,'editLevel']);
    Route::get('editbook/{id}',[HomeController::class,'editBook']);
    Route::get('editchapter/{id}',[HomeController::class,'editChapter']);
    Route::get('editlesson/{id}',[HomeController::class,'editLesson']);
    Route::get('editcontent/{id}',[HomeController::class,'editContent']);
    Route::post('updatelevel',[HomeController::class,'updateLevel'])->name('updatelevel');
    Route::post('updatebook',[HomeController::class,'updateBook'])->name('updatebook');
});





  